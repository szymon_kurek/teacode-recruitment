import './User.css';

import React from 'react';

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {checked: false};
  }

  render() {
    const {checked} = this.state;

    const {
      idx,
      avatar,
      name,
      surname,
      selectUser,
      logSelectedUsers,
    } = this.props;

    return (
      <div
        className="user"
        onClick={() => {
          selectUser(idx, !checked);
          this.setState({checked: !checked});
          setTimeout(logSelectedUsers, 0);
          console.log(this.props.selected);
        }}
      >
        <div
          className="user__image"
          style={{backgroundImage: `url(${avatar})`}}
        />

        <div className="user__name">
          <span>{`${name} ${surname}`}</span>
        </div>

        <input
          className="user__checkbox"
          type="checkbox"
          checked={this.state.checked}
          onChange={() => true}
        />
      </div>
    );
  }
}

export default User;
