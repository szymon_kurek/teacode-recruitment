import '../Users/Users.css';

import React from 'react';

import axios from 'axios';
import User from '../User/User';

import usersJSON from '../../users.json';

class Users extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fetchedUsers: [],
      selectedUsers: [],
      filteredUsers: undefined,
      searchPhrase: '',
    };

    this.selectUser = this.selectUser.bind(this);
    this.logSelectedUsers = this.logSelectedUsers.bind(this);
    this.filterUsers = this.filterUsers.bind(this);
  }

  selectUser(user, checked) {
    const {selectedUsers} = this.state;

    if (checked && !selectedUsers.includes(user)) {
      this.setState({selectedUsers: [...selectedUsers, user]});
    }

    if (!checked) {
      this.setState({
        selectedUsers: selectedUsers.filter((x) => x !== user),
      });
    }
  }

  logSelectedUsers() {
    console.log(this.state.selectedUsers);
  }

  filterUsers() {
    const {fetchedUsers, searchPhrase} = this.state;

    this.setState({
      filteredUsers: fetchedUsers.filter((x) => {
        return (
          x.props.name.includes(searchPhrase) ||
          x.props.surname.includes(searchPhrase)
        );
      }),
    });

    if (searchPhrase === '') {
      this.setState({fetchedUsers: this.state.filteredUsers});
    }
  }

  componentDidMount() {
    const users = usersJSON
      .sort((a, b) => a.last_name.localeCompare(b.last_name))
      .map(({id, first_name, last_name, avatar}) => (
        <User
          name={first_name}
          surname={last_name}
          avatar={avatar}
          idx={id}
          key={id}
          selectedUsers={this.state.selectedUsers}
          selectUser={this.selectUser}
          logSelectedUsers={this.logSelectedUsers}
        />
      ));

    this.setState({fetchedUsers: users});
  }

  render() {
    const {filteredUsers, fetchedUsers} = this.state;

    return (
      <>
        <div className="search">
          <input
            onChange={({currentTarget}) => {
              this.setState({searchPhrase: currentTarget.value});
              setTimeout(this.filterUsers, 0);
            }}
            placeholder="Type in to filter users by first name or last name"
          />
        </div>
        <div className="users"> {filteredUsers || fetchedUsers} </div>
      </>
    );
  }
}

export default Users;
