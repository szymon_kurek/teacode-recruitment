import './App.css';

import React from 'react';
import Header from './Components/Header/Header';

import Users from './Components/Users/Users';

class App extends React.Component {
  // getUsers() {
  //   axios
  //     .get(
  //       `https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`,
  //       {headers: {'Access-Control-Allow-Origin': '*'}}
  //     )
  //     .then((response) => console.log(response));
  // }
  // {
  //   "id": 1,
  //   "first_name": "Suzie",
  //   "last_name": "Kydd",
  //   "email": "skydd0@prnewswire.com",
  //   "gender": "Female",
  //   "avatar": "https://robohash.org/fugiatautemodit.png?size=50x50&set=set1"
  // },

  render() {
    return (
      <div className="App">
        <Header />
        <Users />
      </div>
    );
  }
}

export default App;
