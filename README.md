This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To run the app in development mode you'll have to have installed node and npm on your computer.
You can download it [here](https://nodejs.org/en/download/)

Afterwards open your command prompt and proceed with following steps:

### `git clone <repositoryURL>`

### `npm install`

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

This is the effect of my 2.5 hours of work.

Things that I wasn't able to do:

- Run a request in a browser for users.json (CORS Policy).
- Search causes checkboxes to be unchecked.

Styles are bad, but there was no requirement for styles, so I focused on the logic.
